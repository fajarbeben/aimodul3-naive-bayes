<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<?php 
require 'NaiveBayes.php' ;
$data = [ 
["Wanita", "S2", "Pendidikan", “Tua”], 
["Pria", "S1", "Marketing", “Muda”],
["Wanita", "SMA", "Wirausaha", “Tua”], 
["Pria", "S1", "Profesional", “Tua”],
["Pria", "S2", "Profesional", “Muda”], 
["Pria", "SMA", "Wirausaha", “Muda”], 
["Wanita", "SMA", "Marketing", “Muda”], 
	 
$data_to_predict = ("Wanita", "SMA", "Wirausaha", "Tua"); 
$nb = new NaiveBayes($data, ["Weather", "Car"]); 
echo $nb->run()->predict($data_to_predict);
	
?>

</body>
</html>


